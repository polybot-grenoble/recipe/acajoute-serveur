# Serveur Acajoute
Serveur acajoute est le centre névralgique d'acajoute.  
Il assure la conversion du code, la gestion des projets, les simulations et la connexion des utilisateurs.
## Installation

Le serveur fonctionne avec __NodeJS v18__ et le gestionnaire de paquets __npm__. Ils peuvent être installés [ici](https://nodejs.org/en/download).

```shell
git clone git@gitlab.com:polybot-grenoble/recipe/acajoute-serveur.git
cd acajoute-serveur
npm i
```
Les variables d'environnements suivantes sont __requises__ par le serveur. Elles peuvent être écrites dans un fichier `.env` à la racine du projet.
```shell
OAUTH2_CLIENT_ID="Client app id"
OAUTH2_CLIENT_SECRET="Super secret client secret"
OAUTH2_REDIRECT_URI="http://localhost:8080/oauth2/gitlab/callback"
STORAGE_PATH="./storage/"
REDIRECT_URL="http://localhost:5173"
DB_PATH="./test.db"
```
`OAUTH2_CLIENT_ID` et `OAUTH2_CLIENT_SECRET` sont obtenues en [générant un jeton d'accès gitlab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Les scopes `api`, `read_api`, `read_user`, `read_repository` et `write_repository` doivent être autorisés.

Pour __démarrer__ le serveur, il suffit d'éxecuter la commande suivante : `npm run test`

A noter que la documentation faite sous forme de commentaire de code peut être exportée en HTML avec la commande : `npm run gen_docs`

## Terminaisons
Le client communique avec le serveur à travers des terminaisons accessibles avec EWS, un wrapper Websocket fait maison. Ces terminaisons servent à échanger les données concernant les projets, les clients et les repo Gitlab. Certaines terminaisons sont uniquement accessibles après avoir lié un compte Gitlab à l'utilisateur.
### Liste des terminaisons et commandes
|Terminaison|Commande|Description|Protégé|
| --------- | ------ | --------- | ----- |
|`/clients`|`client_local_login`| Connecte l'utilisateur au serveur | Non |
|`/projects`|`init`| Créé un nouveau projet | Partiellement |
|`/projects`|`save`| Sauvegarde les données d'un projet | Partiellement |
|`/projects`|`load`| Charge les données d'un projet | Partiellement |
|`/projects`|`list`| Liste les projets d'un utilisateur | Non |
|`/projects`|`delete`| Efface un projet | Partiellement |
|`/projects`|`metadata`| Charge les métadonnées d'un projet | Non |
|`/repo`|`init`| Créé un nouveau dépôt sur le compte gitlab de l'utilisateur | Oui |
|`/repo`|`list`| Liste les dépôts gitlab de l'utilisateur | Oui |

## OAuth2
Les utilisateurs ont la possibilité de se connecter avec leur compte Gitlab à l'aide du protocole OAuth2. Cela leur permet de sauvegarder leurs projets dans un dépôt.
Les terminaisons HTTP suivantes sont requises pour mettre en place ce système. 
- `/oauth2/gitlab?clientId={id}` Pour se connecter à Gitlab.
- `/oauth2/gitlab/callback` Requis par Gitlab.
- `/oauth2/gitlab/logout?clientId={id}` Pour se déconnecter de Gitlab.

__NB :__ Dans tous les cas, `{id}` correspond à l'identifiant de l'utilisateur.

## Simulations
Simuler l'éxecution du code est un moyen de le débugger en temps réel. Cela se révèle crucial lors du développement de stratégies pour les robots car on veut pouvoir les tester virtuellement avant de les tester sur les robots.


La conversion du code n'étant pas terminée, l'outil de simulation n'a pas été développé.

## Conversion du code
Cette partie n'est pas aboutie.
Le but était de traduire du code sous forme de bulles, facile à manipuler par tout le monde en un programme fonctionnel.
Pour la suite de ce projet trois fichiers de recherche sont disponible.
Dans certaines fonctions le corps est en commentaire pour permmettre la génération de la documention.
ils sont :
- liste-simple.d.ts
- Abstrac-syntax-tree.old.ts
- suite-instruction


## Types de feuilles

### Fonction

Correspond à un appel de fonction.
N'a qu'un seul précédent et un seul suivant.
Peut avoir plusieurs arguments en entrée ou en sortie.

### Return

Correspond à la fin d'une fonction.
Peut avoir des arguments en entrée.

### Variable

Permet d'accéder à une variable globale.
Ne possède pas de précéent ni de suivant.
A un seul argument de sortie.

### AssigneVariable

Permet d'assigner une variable globale.
N'a qu'un seul précédent et un seul suivant.
A un seul argument d'entrée.

### Caster

Permet le transtypage.
Possède une entrée et une sortie d'argument de types différents.

### Relais

Aide visuelle, n'a aucun impact sur le progamme.
Possède une entrée et une sortie d'argument du même type.

### Event

Début d'arbre évènement. 
Possède un suivant. 
Peut avoir plusieurs sorties d'arguments.

### Flow

Là, il y a du monde.

- `if`:     1 précédent, 
            3 suivants (vrai, faux, suite), 
            1 entrée (condition)

- `while`:  1 précédent, 
            2 suivants (corps, suite), 
            1 entrée (condition)

- `foreach`: 1 précédent, 
            2 suivants (corps, suite), 
            1 entrée (tableau),
            2 sorties (valeur, indice)

- `switch`: 1 précédent,
            n suivants,
            1 entrée (valeur),

- `dowhile`: comme `while`

### Racine

Noeud à l'origine d'une fonction.
Possède un suivant.
Peut avoir plusieurs arguments en sortie.


## Types d'instructions

Une instruction consiste en un type, d'une liste d'arguments et 
potentiellement d'un nom.

### Instructions atomiques

 - `assignation` permet d'affecter une valeur à une variable
 - `appel` est en lien avec l'appel de fonction. le champ "nom" est utilisé ici.
 - `retour` représente une fin de fonction
 - `cast` est utilisé pour le transtypage pour les langages concernés (ex: C), 
    ignoré sinon
 
### Blocs de contrôle 

 - `if` accompagné d'un argument (sa condition)
    - `end_if` ferme le bloc if
    - `else` ouvre le bloc else **à la suite d'un `end_if`**
    - `end_else` ferme le bloc else

 - `while` accompagné d'un argument (sa condition)
    - `end_while` ferme le bloc

 - `dowhile` accompagné d'un argument (sa condition)
    - `end_dowhile` ferme le bloc

 - `switch` accompagné d'un argument (la variable à tester)
    - `case` accompagné d'un argument (la constante de comparaison), ouvre un bloc de comparaison
        - `end_case` ferme le bloc de comparaison
    - `end_switch` ferme **la totalité du bloc contenant les `case`**

 - `foreach` accompagné de trois arguments 
            (le tableau à itérer, la variable contenant la valeur de l'itération, la variable contenant l'indice de l'itération)
    - `end_foreach` ferme le bloc



