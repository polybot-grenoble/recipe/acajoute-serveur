import { getRequest, postRequest, putRequest, deleteRequest } from './API';

/**
 * Fonction qui récupère les données utilisateur GitLab liées au token OAuth.
 * @param accessToken jeton d'accès OAuth.
 * @param cb Réponse de l'API, contenant les données de l'utilisateur (id,nom,pseudo). Vide si la requête n'a pas aboutie.
 */
export function getUserInfo(accessToken: string, cb: (user: gitlabUser | undefined)=>void): void {
    getRequest(`https://gitlab.com/api/v4/user?access_token=${accessToken}`, (data)=>{
        if(!data) return cb(undefined);
        cb({id: data["id"], name: data["name"], username: data["username"]});
    });
}

/**
 * Fonction qui met à jour le token oauth Gitlab à partir du dernier refresh_token.
 * @param refreshToken jeton de rafraichissement Oauth.
 * @param cb Réponse de l'API, contenant le nouveau token (accessToken, refreshToken, expiration, date_de_creation). Vide si la requête n'a pas aboutie.
 */
export function postRefreshedAccessToken(refreshToken: string, cb: (tokenData: gitlabTokenData | undefined)=>void): void {
    postRequest(`gitlab.com`,`/oauth/token?client_id=${process.env.OAUTH2_CLIENT_ID}&client_secret=${process.env.OAUTH2_CLIENT_SECRET}&refresh_token=${refreshToken}&grant_type=refresh_token&redirect_uri=${process.env.OAUTH2_REDIRECT_URI}`, {},(data)=>{
        if(!data) return cb(undefined);
        if(data["error"]){
            console.error("Error while refreshing token: ", data["error"], data["error_description"]);
            return cb(undefined);
        }
        cb({accessToken: data["access_token"], refreshToken: data["refresh_token"], expiry: data["expires_in"], createdAt: data["created_at"]});
    });
}

/**
 * Fonction qui liste les projets Gitlab de l'utilisateur.
 * @param accessToken jeton de l'utilisateur.
 * @param cb Réponse de l'API, contenant la liste des projets de l'utilisateur (id,nom,visibilité,branche). Vide si la requête n'a pas aboutie.
 */
export function getProjects(accessToken: string, cb: (projects: gitlabProject[] | undefined) => void): void {
    getRequest(`https://gitlab.com/api/v4/projects?access_token=${accessToken}&owned=true`, (data)=>{
        if(!data) return cb(undefined);
        let res: gitlabProject[] = [];
        for(let p of data){
            res.push({id: p["id"], name: p["name"], visibility: data["visibility"], defaultBranch: data["default_branch"]});
        }
        cb(res);
    });
}

/**
 * Fonction qui créer un projet gitlab sur le compte de l'utilisateur.
 * @param accessToken jeton de l'utilisateur.
 * @param name Nom du projet
 * @param visibility Visibilité du projet (public, prive, interne).
 * @param cb Réponse de l'API, contenant les informations liées au nouveau projet (id,nom,visibilité,branche). Vide si la requête n'a pas aboutie.
 */
export function postProject(accessToken: string, name: string, visibility: gitlabVisibility, cb: (project: gitlabProject | undefined) => void): void {
    postRequest(`gitlab.com`, `/api/v4/projects?access_token=${accessToken}`, {"name": name, "visibility": visibility}, (data)=>{
        if(!data) return cb(undefined);
        if(data["message"]){
            console.error(`Error while creating project: ${JSON.stringify(data["message"])}`);
            return cb(undefined);
        }
        cb({id: data["id"], name: data["name"], visibility: data["visibility"], defaultBranch: data["default_branch"]});
    })
}

/**
 * Fonction qui récup_re les informations d'un projet gitlab de l'utilisateur.
 * @param accessToken jeton de l'utilisateur.
 * @param projectId Id du projet.
 * @param cb Réponse de l'API, contenant les informations liées au projet (id,nom,visibilité,branche). Vide si la requête n'a pas aboutie.
 */
export function getProject(accessToken: string, projectId: number, cb: (project: gitlabProject | undefined) => void): void {
    getRequest(`https://gitlab.com/api/v4/projects/${projectId}?access_token=${accessToken}`, (data)=>{
        if(!data) return cb(undefined);
        if(data.error){
            console.error(`Cannot get project file: ${data.error}`);
            return cb(undefined);
        }
        cb({id: data["id"], name: data["name"], visibility: data["visibility"], defaultBranch: data["default_branch"]});
    })
}

/**
 * Fonction qui écrit un nouveau fichier dans un projet gitlab.
 * @param accessToken jeton de l'utilisateur.
 * @param projectId Id du projet.
 * @param filePath Chemin du fichier dans le projet.
 * @param branch Branche du projet.
 * @param commit Message du commit.
 * @param fileData Données du fichier.
 * @param cb Réponse de l'API. Vide si la requête a aboutie, message d'erreur sinon.
 */
export function postProjectFile(accessToken: string, projectId: number, filePath: string, branch: string, commit: string, fileData: string, cb: (err: any | undefined) => void): void {
    postRequest(`gitlab.com`, `/api/v4/projects/${projectId}/repository/files/${filePath}?access_token=${accessToken}`, {"branch": branch, "commit_message": commit, "content": fileData}, (data)=>{
        if(!data) return cb(undefined);
        if(data.message) {
            console.error(`Cannot post project ${projectId} file : ${data.message}`);
            return cb(undefined);
        }
        cb(data);
    })
}

/**
 * Fonction qui met à jour un fichier existant dans un projet gitlab.
 * @param accessToken jeton de l'utilisateur.
 * @param projectId Id du projet.
 * @param filePath Chemin du fichier dans le projet.
 * @param branch Branche du projet.
 * @param commit Message du commit.
 * @param fileData Données du fichier.
 * @param cb Réponse de l'API. Vide si la requête a aboutie, message d'erreur sinon.
 */
export function updateProjectFile(accessToken: string, projectId: number, filePath: string, branch: string, commit: string, fileData: string, cb: (err: any | undefined) => void): void {
    putRequest(`gitlab.com`, `/api/v4/projects/${projectId}/repository/files/${filePath}?access_token=${accessToken}`, {"branch": branch, "commit_message": commit, "content": fileData}, (data)=>{
        if(!data) return cb(undefined);
        if(data.message){
            console.error(`Cannot update project ${projectId} file : ${data.message}`);
            return cb(undefined);
        }
        cb(data);
    })
}

/**
 * Fonction qui efface un fichier dans un projet gitlab.
 * @param accessToken jeton de l'utilisateur.
 * @param projectId Id du projet.
 * @param filePath Chemin du fichier dans le projet.
 * @param branch Branche du projet.
 * @param commit Message du commit.
 * @param cb Réponse de l'API. Vide si la requête a aboutie, message d'erreur sinon.
 */
export function deleteProjectFile(accessToken: string, projectId: number, filePath: string, branch: string, commit: string, cb: (res: any | undefined) => void): void {
    deleteRequest(`gitlab.com`, `/api/v4/projects/${projectId}/repository/files/${filePath}?access_token=${accessToken}`, {"branch": branch, "commit_message": commit}, (data)=>{
        if(!data) return cb("Error");
        if(data.message){
            console.error(`Cannot delete project ${projectId} file : ${data.message}`)
            return cb(data.message);
        }
        cb(undefined);
    })
}

/**
 * Fonction qui recupère les données d'un fichier dans un projet gitlab.
 * @param accessToken jeton de l'utilisateur.
 * @param projectId Id du projet.
 * @param filePath Chemin du fichier dans le projet.
 * @param branch Branche du projet.
 * @param cb Réponse de l'API. data contient les données du fichier en texte. err si la requête n'a pas aboutie.
 */
export function getProjectFile(accessToken: string, projectId: number, filePath: string, branch: string, cb: (err: string | undefined, data: any | undefined) => void): void {
    getRequest(`https://gitlab.com/api/v4/projects/${projectId}/repository/files/${filePath}?access_token=${accessToken}&ref=${branch}`, (data)=>{
        if(!data) return cb("No data received.", undefined);
        if(data.error){
            console.error(`Cannot get project file: ${data.error}`);
            return cb(data.error, undefined);
        }
        cb(undefined, Buffer.from(data.content, "base64")?.toString());
    })
}

/** Type qui décrit les données d'un utilisateur gitlab. */
type gitlabUser = {
    id: string;
    name: string;
    username: string;
}

/** Type qui décrit les choix possibles de visibilité pour les projets gitlab. */
export type gitlabVisibility = "private" | "internal" | "public"

/** Type qui décrit les données d'un projet gitlab. */
export type gitlabProject = {
    id: number;
    name: string;
    visibility: gitlabVisibility;
    defaultBranch: string;
}

/** Type qui décrit les données d'un jeton OAuth gitlab. */
type gitlabTokenData = {
    accessToken: string;
    refreshToken: string;
    expiry: number;
    createdAt: number;
}