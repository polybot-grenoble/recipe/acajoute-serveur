import * as gitlab from './gitlab.js';
import https from 'https'

/**
 * Requête REST API get.
 * @param url URL à contacter.
 * @param cb Réponse de la requête.
 */
function getRequest(url: string, cb: (data: any)=>void): void {
    https.get(url, (res)=>{
        let raw = '';
        res.on('data', (d) => {
            raw+=d;
        });
        res.on("end", ()=>{
            try {
                const data = JSON.parse(raw);
                cb(data);
            }catch(err){
                console.error("Failed to parse data from get request ", err);
                return cb(undefined);
            }
        });
        res.on("error", (err)=>{
            console.log("Failed to reach api: ", err);
            return cb(undefined);
        })
    })
}

/**
 * Requête REST API post.
 * @param hostname Adresse IP.
 * @param path Chemin.
 * @param data Données à poster.
 * @param cb Réponse de la requête.
 */
function postRequest(hostname: string, path: string, data: any, cb: (data: any)=>void): void {
    let postData = JSON.stringify(data);

    let options: https.RequestOptions = {
        method: "POST",
        port: 443,
        hostname: hostname,
        path: path,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    };
    
    let req = https.request(options, (res) => {
        let raw = '';
    
        res.on('data', (d) => {
            raw+=d;
        });

        res.on('end', ()=>{
            try {
                const data = JSON.parse(raw);
                cb(data);
            }catch(err){
                console.error("Failed to parse data from post request ", err);
                return cb(undefined);
            }
        });

        res.on('error', (err)=>{
            console.log("Failed to reach api: ", err);
            return cb(undefined);
        })
    });
      
    req.on('error', (err) => {
        console.log("Failed to reach api: ", err);
        return cb(undefined);
    });
    
    req.write(postData);
    req.end();
}

/**
 * Requête REST API put.
 * @param hostname Adresse IP.
 * @param path Chemin.
 * @param data Données à poster.
 * @param cb Réponse de la requête.
 */
function putRequest(hostname: string, path: string, data: any, cb: (data: any)=>void): void {
    let postData = JSON.stringify(data);

    let options: https.RequestOptions = {
        method: "PUT",
        port: 443,
        hostname: hostname,
        path: path,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    };
    
    let req = https.request(options, (res) => {
        let raw = '';
    
        res.on('data', (d) => {
            raw+=d;
        });

        res.on('end', ()=>{
            try {
                const data = JSON.parse(raw);
                cb(data);
            }catch(err){
                console.error("Failed to parse data from post request ", err);
                return cb(undefined);
            }
        });

        res.on('error', (err)=>{
            console.log("Failed to reach api: ", err);
            return cb(undefined);
        })
    });
      
    req.on('error', (err) => {
        console.log("Failed to reach api: ", err);
        return cb(undefined);
    });
    
    req.write(postData);
    req.end();
}

/**
 * Requête REST API delete.
 * @param hostname Adresse IP.
 * @param path Chemin.
 * @param data Données à poster.
 * @param cb Réponse de la requête.
 */
function deleteRequest(hostname: string, path: string, data: any, cb: (data: any)=>void): void {
    let postData = JSON.stringify(data);

    let options: https.RequestOptions = {
        method: "DELETE",
        port: 443,
        hostname: hostname,
        path: path,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    };
    
    let req = https.request(options, (res) => {
        let raw = '';
    
        res.on('data', (d) => {
            raw+=d;
        });

        res.on('end', ()=>{
            try {
                const data = JSON.parse(raw);
                cb(data);
            }catch(err){
                console.error("Failed to parse data from post request ", err);
                return cb(undefined);
            }
        });

        res.on('error', (err)=>{
            console.log("Failed to reach api: ", err);
            return cb(undefined);
        })
    });
      
    req.on('error', (err) => {
        console.log("Failed to reach api: ", err);
        return cb(undefined);
    });
    
    req.write(postData);
    req.end();
}

export {gitlab, getRequest, postRequest, putRequest, deleteRequest};