type Bloc = 
        BlocFonction
    |   BlocAssigne
    |   BlocRetour
    |   BlocOperande
    |   BlocFlux;

type TypeBloc = 
    'fonction' |
    'assigne'  |
    'retour'   |
    'operation';

type BlocFonction = {
    type: "fonction",
    id : string,
    nom : string,
    args: __Argument[],
    lignes: Bloc[],
    out: __Argument[],
};

type BlocAssigne = {
    type: "assigne",
    id: string,
    nom: string,
    valeur : BlocOperande,
};

type BlocRetour = {
    type: "retour",
    id: string,
    nom: string,
    valeur : BlocOperande,
};

type BlocOperande = {
    type: "operation",
    operateur: string,
    args: BlocOperation [],
} | {
    type: "variable",
    args : Argument,
};

/**
 * Pour les flux: 
 *  bloc[0] chemin vrai
 *  bloc[1] chemin faux
 *  le chemin suite est dans la suite du bloc Focntion
 */
type BlocFlux = {
    type: FlowType,
    condition: BlocOperande[],
    lignes: Bloc[][],
} ;

class Pile {
    constructor(...elements) {
      // Initializing the queue with given arguments 
      this.elements = [...elements];
    }
    // Proxying the push/shift methods
    add(...args) {
      return this.elements.unshift(...args);
    }
    sub(...args) {
      return this.elements.shift(...args);
    }
    // Add some length utility methods
    getLength(...args) {
      return this.elements.length;
    }
    isEmpty(){
        return this.elements.length == 0;
    }
    setLength(length) {
      return this.elements.length = length;
    }
}

//Les blocs actifs sont crée un part un en avancant dans l'arbre.
//Les blocs actifs sont : Toutes les Feuilles - {Feuilles: variable, assigne_variable, caster, operateur, relais}
//Tout les blocs actifs utilise une variable temp définit juste avant dans le code de la fonction (un nouveau bloc assigment).
// -> Pour pouvoir utiliser les modifications des variables globales en direct.

    /**
     * ### Fonction permettant le parcours d'un arbre et la création de sont blocs fonction associé
     * - truc
     * @param ArbreParcours L'arbre à parcourir
     * @returns Renvoie le bloc fonction de l'arbre parcourus 
     */
function ParcoursArbreToBlocFonction(ArbreParcours : __Arbre) : BlocFonction {
    
    /**
     * Vérification de la validité de l'arbre
     * c-à-d : Au moins un return dans la fonction
     */

    /**
     *   Dès qu'on a un bloc actif:
     *      Création des blocs assignes qui lui sont liée
     *          On remonte au maximum d'un chemin d'assigne quitte à par la suite utiliser
     * des trucs:(assign tempZ {"+" tempY X})
     *      après ça :
     *          Création du bloc actif K
     *      Suite du parcours...
     **/ 
    let FonctionBlocRetour: BlocFonction;

    /**
     * Copie Statique des constantes de l'arbre
     */
    FonctionBlocRetour.nom = ArbreParcours.nom;
    FonctionBlocRetour.id = ArbreParcours.id;
    FonctionBlocRetour.args = ArbreParcours.inputArgs;
    FonctionBlocRetour.out = ArbreParcours.outputArgs;
    
    let dicoFeuilles: {[key: string]: __Feuille;} = ArbreParcours.feuilles;
    let voyageurFeuille : __Feuille = dicoFeuilles[ArbreParcours.racine];
    
    if(voyageurFeuille.type != 'racine' ) return  null;
    
    let pile : Pile = new Pile;
    /**
     * On remplit la pile
     */
    ParcoursArbreToBlocFonction(voyageurFeuille,pile,dicoFeuilles);
    /* On boucle tant que la pile n'est pas vide*/
    while(!(pile.isEmpty)){
        if(Est_FeuillePassive){

            FonctionBlocRetour.push()
        }
    }



}

/**
 * Parcours Postfixé
 */
function ParcoursRecArbreDepuisFeuille(FeuilleActuel: __Feuille, STACK : Pile,dicoFeuilles :{[key: string]: __Feuille;} ) : void{
    if (FeuilleActuel.type == 'retour'){
        STACK.add(FeuilleActuel.id);
        return;
    }
    for(let fils in FeuilleActuel.args?.in){
        let feuilleAParcourir = dicoFeuilles[fils];
        ParcoursRecArbreDepuisFeuille(feuilleAParcourir,STACK,dicoFeuilles);
        STACK.add(feuilleAParcourir);
    }
    return;
}

    /**
     * ### Vérification de la validité de l'arbre
     * - c-à-d : Au moins un return dans la fonction
     * ---
     * @param ArbreParcours L'arbre que l'on va vérifier
     * @returns la validité de l'arbre en booléen
     */
function VerificationValiditeArbre(ArbreParcours : __Arbre) : boolean {
    
    let valide = false;
    let dico = ArbreParcours.feuilles;
    
    /**
     * À changer pour un parcours en profondeur
     */
    for (let k in dico){
        if(dico[k].type == 'retour'){
            valide = true;
        }
    }

    return valide;
}

/**
 * TODO :
 */


/**
 * ### Fonction qui avec un projet d'arbres effectue la transformation en blocFonction
 * @param ProjetParcours Le projet complet à transformer en blocFonction (faire le main)
 * @returns Un bloc fonction qui est le main d'un programme
 */
function ProjetToBlocFonction(ProjetParcours : __Projet) : BlocFonction;

/**
 * ### Fonction qui détermine si la feuille est passive
 * Une feuille passive est une feuille qui n'as pas besoin d'assign avant
 * elle, dans le traducteur est est directement traduit à la suite des
 * autres actions, par exemple : racine, retour.
 * Attention operateur necessite un assign
 * @param FeuilleDeterminer La feuille à derterminer
 * @returns vrai si la feuille est passive
 */
function Est_FeuillePassive(FeuilleDeterminer: __Feuille) : boolean{
    switch(FeuilleDeterminer.type){
        case 'operateur':
        case 'flux':
            return false      
        default:
            return true;
    }
}

/**
 * fonction qui permet de detecter le début d'une chaine d'operation
 * @param FeuilleOpe 
 * @returns vrai si c'est le début d'une chaine d'operation
 */
function Est_operationSource(FeuilleOpe : __Feuille) : boolean;

function feuilleToBloc(FeuilleActuel : __Feuille) : Bloc{
    /*
        BlocFonction
    |   BlocAssigne
    |   BlocRetour
    |   BlocOperande
    |   BlocFlux;*/

    /*FLUX ou OPERATEUR*/
    if(!Est_FeuillePassive(FeuilleActuel)){
    
    }
    else{ /* dans les autres cas: */
        let newbloc : Bloc;
        switch (FeuilleActuel.type) {
            case 'assignation':
                break;
            case 'caster':
                break;
            case 'fonction':
                break;
            case 'racine':
                break;
            case 'relais':
                break;
            case 'retour':
                break;
            case 'variable':
                break;
            default:
                break;
        }
    }
}

