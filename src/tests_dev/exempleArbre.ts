export let projetFactice: Projet =  {

    id: "PROJEEEEET",
    nom: "Un projet test",
    
    var_globales: {
        "a": { nom: "a", defaut: "True", type: "bool" },
        "q": { nom: "q", defaut: "12", type: "int" }
    },

    fonctions: {
        "Fun": {
            racine: "Fun:racine_0",
            feuilles: {
                "Fun:fonction_0": <FeuilleRacine>{
                    type: "racine",
                    id: "Fun:racine_0",
                    ref: "Fun",
                    flux: {
                        type: "racine",
                        suiv: "Fun:si_0", 
                    },
                    args: {
                        in: [
                            { defaut: "false", nom: "b", type: "bool" },
                            { defaut: "5", nom: "p", type: "int" }
                        ]
                    }
                },
                
                "Fun:si_0": <FeuilleIf> {
                    type: "flux",
                    args: { 
                        in: [ 
                            { defaut: "false", nom: "Condition", type: "bool", valeur: "Fun:ou_0:o:0" } 
                        ] 
                    },
                    id: "Fun:si_0",
                    ref: "Si",
                    flux: {
                        type: "if",
                        vrai: "Fun:retour_0", 
                        faux: "Fun:retour_1",
                        prec: "Fun:racine_0"
                    }
                },

                "Fun:retour_0": <FeuilleRetour>{
                    type: "retour",
                    args: {
                        in: [ { type: "int", defaut: "0", nom: "o", valeur: "Fun:add_0:o:0" } ]
                    },
                    id: "Fun:retour_0",
                    ref: "Retour",
                    flux: {
                        type: "retour",
                        prec: "Fun:si_0:0",
                    }                
                }, 

                "Fun:retour_1": <FeuilleRetour>{
                    type: "retour",
                    args: {
                        in: [ { type: "int", defaut: "0", nom: "o", valeur: "Fun:mult_0:o:0" } ]
                    },
                    id: "Fun:retour_1",
                    ref: "Retour",
                    flux: {
                        type: "retour",
                        prec: "Fun:si_0:1"
                    }
                }, 

                "Fun:add_0": <FeuilleOperateur> {
                    type: "operateur",
                    id: "Fun:add_0",
                    ref: "Addition",
                    args: {
                        in: [ 
                            { defaut: "0", nom: "", type: "int", valeur: "Fun:relais_0:o:0" },
                            { defaut: "0", nom: "", type: "int", valeur: "Fun:varglob_1:o:0" } 
                        ],
                        out: [ { defaut: "0", nom: "", type: "int" } ]
                    }
                },
                
                "Fun:mult_0": <FeuilleOperateur> {
                    type: "operateur",
                    id: "Fun:mult_0",
                    ref: "Multiplication",
                    args: {
                        in: [
                            { defaut: "0", nom: "", type: "int", valeur: "Fun:relais_0:o:0" },
                            { defaut: "0", nom: "", type: "int", valeur: "Fun:add_0:o:0" } 
                        ],
                        out: [ { defaut: "0", nom: "", type: "int" } ],
                    }
                },

                "Fun:ou_0": <FeuilleOperateur> {
                    type: "operateur",
                    id: "Fun:add_0",
                    ref: "Addition",
                    args: {
                        in: [ 
                            { defaut: "0", nom: "", type: "bool", valeur: "Fun:racine_0:o:0" },
                            { defaut: "0", nom: "", type: "bool", valeur: "Fun:varglob_0:o:0" } 
                        ],
                        out: [ { defaut: "0", nom: "", type: "int" } ]
                    }
                },

                "Fun:relais_0": <FeuilleRelais> {
                    type: "relais",
                    id: "Fun:realais_0",
                    ref: "",
                    args: {
                        in: [ { defaut: "", nom: "", type: "int", valeur: "Fun:racine_0:o:0" } ],
                        out: [ { defaut: "", nom: "", type: "int" } ],
                    }
                },

                "Fun:varglob_1": <FeuilleVariable> {
                    id: "Fun:varglob_1",
                    ref: "",
                    type: "variable",
                    args: {
                        out: [ { defaut: "0", nom: "q", type: "int", valeur: "q" } ],
                    }
                },

                "Fun:varglob_0": <FeuilleVariable> {
                    type: "variable",
                    id: "Fun:varglob_0",
                    ref: "",
                    args: {
                        out: [ { defaut: "0", nom: "a", type: "bool", valeur: "a" } ],
                    }
                },

            }
        }
    }

}