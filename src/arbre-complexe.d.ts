type TypeFeuille = 
    'racine' | 
    'retour' | 
    'fonction' | 
    'variable' | 
    'assignation' | 
    'caster' | 
    'operateur' | 
    'relais' | 
    'event' | 
    'flux' | 
    'undefined';

type FlowType = 
    'normal' | 
    'retour' |
    'racine' | 
    'if' | 
    'while' | 
    'foreach' | 
    'switch';

/* === Types génériques === */

type __Argument = {
    type: string;
    nom: string;
    valeur?: string;
    defaut?: string;
};

type __Flux = {
    type: FlowType;
    in: { nom?: string, connexion?: string }[],
    out: { nom?: string, connexion?: string }[],
};

type __Feuille = {
    
    type: TypeFeuille;      // Type de feuille
    id: string;             // Identifiant unique
    ref: string;            // Référencement pour l'éditeur / le traducteur sur l'identité de la feuille
    flux?: __Flux;
    args?: {                // __Arguments de la feuille
        in?: __Argument[];    // __Arguments d'entrée
        out?: __Argument[];   // Valeurs de retour
    };

};

type __Arbre = {
    
    id: string;
    nom: string;
    racine: string;

    inputArgs: __Argument[];
    outputArgs: __Argument[];
    
    feuilles: { 
        [key: string]: __Feuille;
    };

}

type __Projet = {

    id: string;
    nom: string;
    var_globales: { [key: string]: __Argument };
    fonctions: { [key: string]: __Arbre };

}