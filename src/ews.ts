import {WebSocketServer, WebSocket} from 'ws';
import crypto from 'crypto';
import {Server} from 'http';

export class EWS {

    private server?: WebSocketServer;
    private parent?: EWS;

    private middleware: Map<string, callbackMiddleware[]>;
    private redirections: Map<string, EWS>;
    private endpoints: Map<string, callBackEndpoint>;
    private clients: Map<string, WebSocket>;

    /**
     * Websocket wrapper to perform Express like process on packets.
     * @param HTTPServer If specified, will listen over it. Warning : root will be /socket and listen function will be disabled.
     * @param parent Parent instance. Useful for more complex infrastructure.
     */
    constructor(HTTPServer?: Server, parent?: EWS){
        this.middleware = new Map<string, callbackMiddleware[]>;
        this.redirections = new Map<string, EWS>();
        this.endpoints = new Map<string, callBackEndpoint>();
        this.clients = new Map<string, WebSocket>();

        if(parent){
            this.parent = parent;
            return;
        }

        if(HTTPServer){
            this.server = new WebSocketServer({ server: HTTPServer });
            this.server.on('connection', (ws)=>{this.handleSocket(ws)});
        }
    }

    /**
     * Redirect any packet to another EWS instance.
     * @param path Path to redirect.
     * @param instance EWS receiver instance.
     */
    redirect(path: string, instance: EWS): void {
        if(this.redirections.has(path)){
            console.error(`'${path}' redirection is already defined.`);
            return;
        }
        this.redirections.set(path, instance);
    }

    /**
     * Setup packet endpoint on a path. It's the packet's final destination.
     * @param path Path to listen to.
     * @param cb Process the packet here.
     */
    on(path: string, cb: callBackEndpoint): void {
        if(this.endpoints.has(path)){
            console.error(`'${path}' endpoint is already defined.`);
            return;
        }

        this.endpoints.set(path, cb);
    }

    /**
     * Setup a packet filter on a specified path.
     * @param path Path to filter.
     * @param cb Return true to forward a packet to the next step. Return false to cancel it.
     */
    use(path: string, cb: callbackMiddleware): void {
        let list = this.middleware.get(path);

        if(list != undefined){
            list.push(cb);
            return;
        }

        list = [cb];
        this.middleware.set(path, list);
    }

    /**
     * Send a packet to every clients.
     * @param command Command.
     * @param args Arguments linked to the command.
     */
    broadcast(command?: string, args?: string[]): void {
        if(this.parent){
            this.parent.broadcast(command, args);
            return;
        }

        for(let [id, ws] of this.clients){
            this.send(id, command, args);
        }
    }

    /**
     * Send a packet to a client.
     * @param socketId Id of the client to send the packet.
     * @param command Command.
     * @param args Arguments linked to the command.
     */
    send(socketId: string, command?: string, args?: string[]): void {
        if(this.parent){
            this.parent.send(socketId, command, args);
            return;
        }

        let c = this.clients.get(socketId);
        if(!c) return;

        let h: packetHeader = {path: "/", serverPath: "/", socketId: socketId, packetId: crypto.randomUUID().toString()};
        let p: packet = {header: h, command: command, args: args};

        c.send(JSON.stringify(p));

        console.log("sent packet")
    }

    /**
     * Start listening on specified port.
     * @param port Port to listen to.
     */
    listen(port: number): void {
        if(this.parent){
            console.error('Cannot listen has a child. Packets will be forwarded from parent');
            return;
        }

        if(this.server){
            console.error(`This instance is already initiated.`);
            return;
        }

        this.server = new WebSocketServer({port: port});

        this.server.on('listening', ()=>{
            console.log(`Server listening on port ${port}`);
        });

        this.server.on('connection', (ws)=>{this.handleSocket(ws)});
    }

    /**
     * Packet processing. Forwards any packet throught the right filters and endpoints.
     * @param p Packet to forward.
     */
    private handlePacket(p: packet): void {
        //Handle middlewares
        for(let [path, mw] of this.middleware){
            if(p.header.serverPath.startsWith(path)){
                for(let m of mw){
                    if(!m(p)){
                        console.log(`Packet ${p.header.packetId} cancelled by a middleware from '${path}'`);
                        return;
                    }
                }
            }
        }
        
        //Handle redirections
        for(let [path, redirect] of this.redirections){
            if(p.header.serverPath.startsWith(path)){
                p.header.serverPath = p.header.serverPath.slice(path.length);
                redirect.handlePacket(p);
                console.log(`Packet ${p.header.packetId} redirected to ${path}`);
                return;
            }
        }

        //Handle endpoints
        for(let [path, endpoint] of this.endpoints){
            if(path == p.header.serverPath){
                endpoint(p);
                return;
            }
        }

        console.error(`Packet ${p.header.packetId} did not reach is destination.`);
    }

    /**
     * Initiate the connection with a new websocket client.
     * Assigns an id. Setup error, logout and message listeners.
     * @param ws Client websocket.
     */
    private handleSocket(ws: WebSocket): void {
        //clientId
        let id: string = crypto.randomUUID().toString();
        this.clients.set(id, ws);
        console.log(`Socket ${id} logged in.`);

        this.send(id, Command.CLIENT_INIT_ID, [id]);

        //close handler
        ws.on('close', (code)=>{
            let id = getIdfromSocket(this.clients, ws);
            if(!id) return;
            
            console.log(`Socket ${id} logged out. Code: ${code}`);
            this.clients.delete(id);
        })

        //error handler
        ws.on('error', (err)=>{
            let id = getIdfromSocket(this.clients, ws);
            if(!id) return;
            
            console.log(`Socket ${id} raised error: ${err}`);
        })

        //message handler
        ws.on('message', (data)=>{
            let id = getIdfromSocket(this.clients, ws);
            if(!id) return;

            this.handlePacket(<packet>JSON.parse(data.toString()));
        })
    }

}

/**
 * Find a socketId with a Socket object.
 * @param map Socket map.
 * @param socket Socket to search for.
 * @returns Id of the socket. Empty if not found.
 */
function getIdfromSocket(map: Map<string, WebSocket>, socket: WebSocket): string {
    for(let [key, value] of map.entries()){
        if(value==socket) return key;
    }
    return "";
}

/**
 * Return true to keep going, false to cancel packet.
 */
export type callbackMiddleware = (packet: packet) => boolean;
export type callBackEndpoint = (packet: packet) => void;

/**
 * Packet header. Contains its path, id, clientId and socketId.
 */
export type packetHeader = {
    packetId: string;
    clientId?: string;
    socketId: string;
    path: string;
    serverPath: string;
}

/**
 * Packet exchanged between the server and the clients. Contains the interesting data for the users.
 */
export type packet = {
    header: packetHeader;
    command?: string;
    args?: string[];
};

/**
 * Internal command used to manage user's session.
 */
export const enum Command {
    CLIENT_INIT_ID="client_init_id",
    CLIENT_LOCAL_LOGIN="client_local_login",
    CLIENT_GITLAB_LOGIN="client_gitlab_login",
    ERROR="error"
}