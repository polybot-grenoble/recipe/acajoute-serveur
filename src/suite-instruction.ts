type id = string;

type programme = {
  type : 'programme',
  adresse : id,
  nom : string,
  intput: (__Argument | operateur)[],
  ligne? : fonction[][];
  output: (__Argument | operateur)[],
}

type fonction = {
    nom : string;
    adresse : id;
    input : (__Argument | operateur)[];
    output: __Argument[];
}

type operateur = {
    _typeObjet:'operateur';
    adresse : id;
    operande : string;
    arguments : (__Argument | operateur)[];
}

/**
 * Un objet de Pile avec les méthodes de gestion associé
 */
class Pile<T> {
    elements : T[];
    constructor(...elements: T[]) {
      /*Initialisation de la pile avec les arguments donnés*/
      this.elements = [...elements];
    }

    /*Méthode de gestion de la pile*/
    add(...args : T[]) {
      return this.elements.unshift(...args);
    }
    sub(){
      return <T> this.elements.shift();
    }
    
    /*Méthode d'analyse de la pile*/
    getLength() {
      return this.elements.length;
    }
    isEmpty(){
        return this.elements.length == 0;
    }
}

/**
 * ### Une fonction qui fait un parcours recursif postfixé de l'arbre dans une pile
 * @param FeuilleActuel la feuille de départ du parcours
 * @param STACK une pile qui stocke tout les feuilles dans le flux de l'arbre
 * @param dicoFeuilles le dictionnaire de toutes les feuilles de l'arbre
 * @returns ne retourne pas de valeur mais rempli le stack par effect de bord
 */
function ParcoursRecArbreDepuisFeuille(FeuilleActuel: __Feuille, STACK : Pile<__Feuille>, dicoFeuilles :{[key: string]: __Feuille;} ) : void{
  if (FeuilleActuel.type == 'retour' || !(FeuilleActuel.flux?.out) ){
    // let {id} = decomposeId(FeuilleActuel.adresse);
    STACK.add(FeuilleActuel);
    return;
  }
  /*
  for(let fils = 0 ; fils > FeuilleActuel.flux?.out.length ; fils++){
    let filsID : id = FeuilleActuel.flux?.out[fils];
    let {id} = decomposeId(filsID);
    let feuilleAParcourir = dicoFeuilles[id];
    ParcoursRecArbreDepuisFeuille(feuilleAParcourir,STACK,dicoFeuilles);
    STACK.add(feuilleAParcourir);
  }
  */
  return;
}

/**
 * ### Une fonction qui parcours l'arbre et le transforme en lignes de fonctions
 * @param ArbreActuel L'arbre a transformé en programme
 * @returns un programme (une liste de fonction pour le moment :!: les arguments temporaires ne sont pas encore gérer)
 */
function fonctionToProgramme(ArbreActuel : __Arbre) : programme {
    /**
     * initialisation statique
     */
  let prog : programme = {
    type : 'programme',
    adresse : ArbreActuel.id,
    nom : ArbreActuel.nom,
    intput : ArbreActuel.inputArgs,
    output : ArbreActuel.outputArgs,
  };
   
  let PileDuFlux: Pile<__Feuille> = new Pile();

  /* Dictionaire des feuilles de l'arbre */
  let ArbreDico = ArbreActuel.feuilles;

  /*Rempli une pile du parcours de l'arbre*/
  ParcoursRecArbreDepuisFeuille(ArbreDico[ArbreActuel.racine],PileDuFlux,ArbreDico);

  let FiledeVisite : __Feuille[] = [];
  while( !(PileDuFlux.isEmpty()) ){
    let feuilleActuel : __Feuille = PileDuFlux.sub();
    FiledeVisite.push(feuilleActuel);

    switch(feuilleActuel.type){
      case 'fonction':
        let {id} = decomposeId(feuilleActuel.id);
        let fct : fonction = {
          nom : feuilleActuel.ref,
          adresse : id,
          input : [],
          output : [],
        }

        /*Ajout des arguments de la fonction*/
        if (!(feuilleActuel.args?.in)){
          break;
        }
        for(let i = 0 ; i< feuilleActuel.args?.in?.length ; i++){
          
          if(!(feuilleActuel.args?.in[i].valeur)){
            
            // fct.input[i] = parcoursRecJusquaRacineArgument(feuilleActuel.args?.in[i].valeur, FiledeVisite,ArbreDico);
          } else {
            fct.input[i] = feuilleActuel.args?.in[i];
          }
        }
        /*Lignes additionnel pour la completion de l'argument de sortie*/
        /*A faire*/


        default:

          break;
        }/**Fin du switch */

    }/**Fin du while */

    return prog;
}

/**
 * ### Une fonction qui doit parcourir le chemin de l'argument rentrant dans la fonction
 * @param precedent La clef d'une feuille du dictionnaire de l'arbre
 * @param FiledeFeuilleVisite La liste des feuilles déjà visiter
 * @param dicoFeuilles le dictionnaire de toutes les feuilles de l'arbre
 * @returns revoie un argument ou, l'opération / suite d'operation qui rentre en argument
 */
function parcoursRecJusquaRacineArgument(precedent:id, FiledeFeuilleVisite: __Feuille[], dicoFeuilles :{[key: string]: __Feuille;},) : /*(operateur | __Argument)*/void {
  let {id,i} = decomposeId(precedent);
  let feuilleActuel = dicoFeuilles[id];
  // if(feuilleActuel.type == 'caster' || feuilleActuel.type == 'relais' || feuilleActuel.type =='assignation'){
  //   // let {id} = decomposeId(feuilleActuel.args?.in[0]);
  //   // let feuillePrecedante : __Feuille = dicoFeuilles[id];
  //   // return parcoursRecJusquaRacineArgument(feuillePrecedante,FiledeFeuilleVisite,dicoFeuilles);
  //   return feuilleActuel.args?.in[i];
  // }
  // if(feuilleActuel.type == 'racine' || feuilleActuel.type == 'variable' ){
  //   return feuilleActuel.args?.out[i];
  // }
  
  // if(feuilleActuel.type == 'fonction'){
  //   /**a remplir */
  // }
  // switch(feuilleActuel.type){
  //   case 'relais':
  //   case 'caster':
  //   case 'assignation':
  //    return feuilleActuel.args?.in[i];
  //   case 'racine':
  //   case 'variable':
  //     return feuilleActuel.args?.out[i];
  //   case 'fonction':
  //     /**A remplir avec la liste de */
  //   case 'operateur':
  //     /**Création de l'operateur puis parcours du menbre de gauche et du membre de droite */
  //     let operation : operateur;
  //     operation.operande = feuilleActuel.args[0].default;

  // }

}


function decomposeId (rawPath: string): { id: string, mode: string, i: number } {

  let [ a,b,c, mode, n ] = rawPath.split(":");
  return { 
      id: `${a}:${b}:${c}`,
      i: parseInt(n),
      mode
  }

}
