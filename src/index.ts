/** NodeJS import to setup express, passport, utilities, db and ews. */
import express, { Request } from 'express'
import {createServer} from 'http'
import passport from 'passport'
import OAuth2Strategy from 'passport-oauth2'
import dotenv from 'dotenv'
import morgan from 'morgan'

import { Command, EWS } from './ews.js';
import * as DB from './db/DB.js';
import * as API from "./api/API.js";
import { LAFM } from './afm.js';
import { checkToken, handleClientsEndpoint, handleProjectsEndpoint, handleRepositoriesEndpoint } from './endpoints.js';

dotenv.config();

/** Initiate server instances. */
const port = 8080;
const app = express();
const server = createServer(app);
const socket = new EWS(server);
const database = DB.openClientDB(<string>process.env.DB_PATH);

/** Express config */
app.use(morgan("tiny"));
app.use(express.json())
app.use(express.urlencoded({extended: false}));
app.use(passport.initialize());

/** OAuth strategy. */
passport.use(new OAuth2Strategy({
    authorizationURL: "https://gitlab.com/oauth/authorize",
    tokenURL: "https://gitlab.com/oauth/token",
    clientID: <string> process.env.OAUTH2_CLIENT_ID,
    clientSecret: <string> process.env.OAUTH2_CLIENT_SECRET,
    callbackURL: <string> process.env.OAUTH2_REDIRECT_URI,
    scope: ["api", "read_api", "read_user", "read_repository", "write_repository"],
    scopeSeparator: ' ',
    passReqToCallback: true
    }, (req: Request, accessToken: string, refreshToken: string, results: any, profile: any, cb: any)=>{
        if(!req.query.state) return cb("Specify a client id.", null);

        /** Process and save client data after successfull oauth login. */
        API.gitlab.getUserInfo(accessToken, (user)=>{
            if(!user) return cb("Internal error: Failed to get user info from Gitlab API.", null);

            DB.clientsSet.addRemoteSession(database, <string> req.query.state, user.id, "gitlab", (res)=>{
                DB.clientsSet.setName(database, <string> req.query.state, user.name, (name)=>{
                    DB.clientsSet.addOAuth2Request(database, {clientId: <string> req.query.state, accessToken: results.access_token, refreshToken: refreshToken, createdOn: results.created_at, expireAt: results.expires_in, origin: "gitlab"}, (oauth)=>{
                        if(!oauth) return cb("Could not register oauth key.", null);
                        cb(null, {id: req.query.state});
                    })
                })
            })
        });
    }
  )
)

/** HTTP endpoints : OAuth */

app.get('/oauth2/gitlab', (req,res,next)=>{
    if(!req.query.clientId){
        res.send(400);
        return;
    }
    passport.authenticate('oauth2', {state: <string> req.query.clientId, session: false})(req,res,next);
})
app.get('/oauth2/gitlab/callback', passport.authenticate('oauth2', {successRedirect: process.env.REDIRECT_URL, failureRedirect: '/oauth2/gitlab', session: false}));

app.get('/oauth2/gitlab/logout', (req,res)=>{
    if(!req.query.clientId){
        res.send(400);
        return;
    }

    DB.clientsRm.rmRemoteSession(database, <string> req.query.clientId, "gitlab", (deleted)=>{
        if(!deleted) return res.send(500);

        DB.clientsRm.rmAllOAuth2Requests(database, <string> req.query.clientId, (cleared)=>{
            if(!cleared) return res.send(500);

            res.redirect(<string>process.env.REDIRECT_URL);
            console.log("User logged out.");
        })
    })
})

/** Start Express server */
server.listen(port, ()=>{console.log(`acajoute backend running on port ${port}`)});

/** Socket endpoints : Data */
socket.on('/repo', (p)=>{
    checkToken(<string>p.header.clientId, database, (token)=>{
        if(!token){
            console.error("Packet blocked. Could not get user's token.");
            socket.send(p.header.socketId, Command.ERROR, [`Request ${p.command} on path ${p.header.path} blocked. You must be logged in.`])
            return;
        }

        handleRepositoriesEndpoint(token, p, socket);
    })
})
socket.on('/projects', (p)=>{handleProjectsEndpoint(p, socket, database);})
socket.on('/clients', (p)=>{handleClientsEndpoint(p, socket, database);})

/** Filter incoming packets. */
socket.use('/', (p)=>{
    if(!p.command){
        socket.send(p.header.socketId, Command.ERROR, [`Request blocked. You must specify a command.`])
        console.error('Packet blocked. Requires clientId.');
        return false;
    }

    if(p.command == Command.CLIENT_LOCAL_LOGIN || p.command == Command.CLIENT_INIT_ID) return true;

    if(!p.header.clientId){
        socket.send(p.header.socketId, Command.ERROR, [`Request ${p.command} blocked. You must be logged in.`])
        console.error('Packet blocked. Requires clientId.');
        return false;
    }

    return true;
})

/** Clean expired local projects. */
setInterval(()=>{new LAFM(database).clear()}, 1000*60*60);