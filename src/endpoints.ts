import { Database } from "sqlite3";
import { Command, EWS, packet } from "./ews";
import * as API from "./api/API";
import * as DB from "./db/DB";
import { OAuthRequest, origin } from './db/DB';
import { GAFM, LAFM } from "./afm";
import { gitlabVisibility } from "./api/gitlab";
import { AFM } from "./afm";
import { randomUUID } from "crypto";

/**
 * Traite les paquets clients liés à la gestion des projets.
 * @param p Paquet recu du client.
 * @param socket Instance EWS. 
 * @param database Base de données.
 */
export function handleProjectsEndpoint(p: packet, socket: EWS, database: Database): void {
    switch(p.command){
        case 'init':
            if(!p.args || p.args.length < 3){
                socket.send(p.header.socketId, "error", ['Specifiy required arguments to init project. id, name and origin.'])
                return;
            }

            DB.project.addProject(database, randomUUID().toString(), p.args[1], p.args[0], <origin>p.args[2], (project)=>{
                if(!project){
                    socket.send(p.header.socketId, "error", ['Failed to init project. Internal DB error.']);
                    return;
                }

                if(!(<string[]>p.args)[3]){
                    socket.send(p.header.socketId, 'project_init', [JSON.stringify(project)]);
                    return;
                }

                DB.project.updateProjectOrigin(database, project.id, "gitlab", parseInt((<string[]>p.args)[3]), (updated)=>{
                    if(!updated){
                        socket.send(p.header.socketId, 'error', ['Could not init project. Internal API error.']);
                        return;
                    }

                    project.origin = "gitlab";
                    project.repoId = parseInt((<string[]>p.args)[3]);
                    socket.send(p.header.socketId, 'project_init', [JSON.stringify(project)]);
                })
            })
            break;
        case 'save':
            if(!p.args || p.args.length < 3){
                socket.send(p.header.socketId, "error", ['Specifiy required arguments to save project. id, project, data and origin.'])
                return;
            }

            DB.project.getProject(database, p.args[1], (project)=>{
                if(!project){
                    socket.send(p.header.socketId, "error", ["Could not save project: Does not exists."])
                    return;
                }

                let afm: AFM;

                switch(project.origin){
                    case "gitlab":
                        checkToken(<string>p.header.clientId, database, (accessToken)=>{
                            if(!accessToken){
                                console.error("Packet blocked. Could not get user's token.");
                                socket.send(p.header.socketId, Command.ERROR, [`Request ${p.command} on path ${p.header.path} blocked. You must be logged in.`])
                                return;
                            }

                            afm = new GAFM(accessToken.accessToken, database);
                            afm.save(project.id, (<string[]>p.args)[2], (<string[]>p.args)[3], (err)=>{
                                if(err){
                                    socket.send(p.header.socketId, "error", [`Could not save project: ${err}`]);
                                    return;
                                }

                                socket.send(p.header.socketId, 'project_save', ['Project saved']);
                            })
                        })
                        
                        break;
                    case "local":
                        afm = new LAFM(database);
                        afm.save(project.id, (<string[]>p.args)[2], "", (err)=>{
                            if(err){
                                socket.send(p.header.socketId, "error", [`Could not save project: ${err}`]);
                                return;
                            }

                            socket.send(p.header.socketId, 'project_save', ['Project saved']);
                        })
                        
                        break;
                    default:
                        socket.send(p.header.socketId, 'error', ["Could not save project: Invalid origin."]);
                        break;
                }
            })
            break;
        case 'load':
            if(!p.args || p.args.length != 1){
                socket.send(p.header.socketId, "error", ["Specifiy required arguments"])
                return;
            }

            DB.project.getProject(database, p.args[0], (project)=>{
                if(!project){
                    socket.send(p.header.socketId, "error", ["Could not load project: Does not exists."])
                    return;
                }

                switch(project.origin){
                    case "gitlab":
                        checkToken(<string>p.header.clientId, database, (accessToken)=>{
                            if(!accessToken){
                                console.error("Packet blocked. Could not get user's token.");
                                socket.send(p.header.socketId, Command.ERROR, [`Request ${p.command} on path ${p.header.path} blocked. You must be logged in.`])
                                return;
                            }

                            new GAFM(accessToken.accessToken, database).load(project.id, (err,projectData)=>{
                                if(err){
                                    socket.send(p.header.socketId, 'error', [`Could not load project: ${err}`]);
                                    return;
                                }
                                if(!projectData){
                                    socket.send(p.header.socketId, 'error', ['Could not load project: Project data not found.']);
                                    return;
                                }
    
                                socket.send(p.header.socketId, 'project_load', [JSON.stringify(project), <string>projectData]);
                            });
                    
                        })
                        
                        break;
                    case "local":
                        new LAFM(database).load(project.id, (err,projectData)=>{
                            if(err){
                                socket.send(p.header.socketId, 'error', [`Could not load project: ${err}`]);
                                return;
                            }
                            if(!projectData){
                                socket.send(p.header.socketId, 'error', ['Could not load project: Project data not found.']);
                                return;
                            }

                            socket.send(p.header.socketId, 'project_load', [JSON.stringify(project), <string>projectData]);
                        });
                        break;
                    default:
                        socket.send(p.header.socketId, 'error', ["Could not load project: Invalid origin."]);
                        break;
                }
            })
            break;
        case 'list':
            DB.project.getProjects(database, <string>p.header.clientId, (projects)=>{
                socket.send(p.header.socketId, "projects_list", [JSON.stringify(projects)]);
            })
            break;
        case 'delete':
            if(!p.args || p.args.length != 1){
                socket.send(p.header.socketId, "error", ['Specifiy required arguments to delete project. id.'])
                return;
            }
            DB.project.getProjects(database, <string>p.header.clientId, (projects)=>{
                if(!projects){
                    socket.send(p.header.socketId, "error", ['Cannot delete project. Does not exists.']);
                    return;
                }

                let project: DB.Project | undefined = undefined;
                for(let pr of projects){
                    if(pr.id == (<string[]>p.args)[1]){
                        project = pr;
                        break;
                    }
                }
                if(!project){
                    socket.send(p.header.socketId, "error", ['Cannot delete project. Does not exists.']);
                    return;
                }
                
                switch(project.origin){
                    case 'gitlab':
                        checkToken(<string>p.header.clientId, database, (accessToken)=>{
                            if(!accessToken){
                                console.error("Packet blocked. Could not get user's token.");
                                socket.send(p.header.socketId, Command.ERROR, [`Request ${p.command} on path ${p.header.path} blocked. You must be logged in.`])
                                return;
                            }
                            new GAFM(accessToken.accessToken, database).delete((<DB.Project>project).id,(err)=>{
                                if(err){
                                    socket.send(p.header.socketId, "error", [`Could not delete project. ${err}`]);
                                    return;
                                }
                                DB.project.deleteProject(database, (<string[]>p.args)[1], (done)=>{
                                    if(!done){
                                        socket.send(p.header.socketId, "error", ['Cannot delete project. Does not exists.']);
                                        return;
                                    }
                                    socket.send(p.header.socketId, 'project_delete', ['Project deleted.']);
                                })
                            })
                        })
                        break;
                    case 'local':
                        new LAFM(database).delete(project.id,(err)=>{
                            if(err){
                                socket.send(p.header.socketId, "error", [`Could not delete project. ${err}`]);
                                return;
                            }
                            DB.project.deleteProject(database, (<string[]>p.args)[1], (done)=>{
                                if(!done){
                                    socket.send(p.header.socketId, "error", ['Cannot delete project. Does not exists.']);
                                    return;
                                }
                                socket.send(p.header.socketId, 'project_delete', ['Project deleted.']);
                            })
                        })
                        break;
                }
            })
            break;
        case 'metadata':
            if(!p.args || p.args.length != 1){
                socket.send(p.header.socketId, "error", ["Specifiy required arguments. project_id."]);
                return;
            }
            DB.project.getProject(database, p.args[0], (project)=>{
                if(!project){
                    socket.send(p.header.socketId, 'err', ['Could not get project metadata: Internal DB error.']);
                    return;
                }

                socket.send(p.header.socketId, 'project_meta', [JSON.stringify(project)]);
            });
            break;
    }
}

/**
 * Traite les paquets clients liés à la gestion des projets gitlab.
 * @param accessToken Jeton OAuth du client.
 * @param p Paquet reçu du client.
 * @param socket Instance EWS. 
 */
export function handleRepositoriesEndpoint(accessToken: OAuthRequest, p: packet, socket: EWS): void {
    switch(p.command){
        case 'list':
            API.gitlab.getProjects(accessToken.accessToken, (gitProjects)=>{
                if(!gitProjects){
                    socket.send(p.header.socketId, 'error', ['Cannot list repo: Internal API error.']);
                    return;
                }

                socket.send(p.header.socketId, 'repo_list', [JSON.stringify(gitProjects)]);
            })
            break;
        case 'init':
            if(!p.args || p.args.length != 2){
                socket.send(p.header.socketId, 'error', ['Cannot init repo: Specify name and visibility.']);
                return;
            }

            API.gitlab.postProject(accessToken.accessToken, p.args[0], <gitlabVisibility>p.args[1], (gitProject)=>{
                if(!gitProject){
                    socket.send(p.header.socketId, 'error', [`Cannot init repo: Internal API error.`]);
                    return;
                }

                socket.send(p.header.socketId, 'repo_init', [JSON.stringify(gitProject)]);
            })
            break;
    }
}

/**
 * Traite les paquets clients liés à l'authentification locale des clients.
 * @param accessToken Jeton OAuth du client.
 * @param p Paquet reçu du client.
 * @param socket Instance EWS. 
 */
export function handleClientsEndpoint(p: packet, socket: EWS, database: Database): void {
    switch(p.command){
        case Command.CLIENT_LOCAL_LOGIN:
            if(!p.args || !p.args[0]) return;
            DB.clientsGet.getClient(database, p.args[0], (check)=>{
                if(!check){
                    console.log('DATABASE: Registering new user.');
                    if(!p.args) return;
                    if(!p.args[0]) return;
                    DB.clientsSet.addClient(database, p.args[0], (newClient)=>{
                        if(!newClient){
                            console.error("DATABASE: Failed to create user.");
                            socket.send(p.header.socketId, Command.ERROR, ["Failed to create user. Try again later."]);
                            return;
                        }
                        
                        //send new client data in response
                        socket.send(p.header.socketId, Command.CLIENT_LOCAL_LOGIN, [JSON.stringify(newClient), 'local']);
                        console.log('DATABASE: New user registered.')
                    })
                }else{
                    DB.clientsGet.getRemoteSession(database, check.id, (session)=>{
                        //send client data in response
                        console.log('DATABASE: New login.')
                        if(!session){
                            socket.send(p.header.socketId, Command.CLIENT_LOCAL_LOGIN, [JSON.stringify(check), 'local']);
                            return;
                        }
                        socket.send(p.header.socketId, Command.CLIENT_LOCAL_LOGIN, [JSON.stringify(check), session.origin]);
                    })
                }
            });
            break;
    }
}

/**
 * Vérifie la validité de la session de connexion et du jeton oauth d'un utilisateur et met à jour le jeton si besoin.
 * @param clientId Id du client.
 * @param database Base de données.
 * @param cb Retourne le jeton OAuth valide du client. Vide sinon.
 */
export function checkToken(clientId: string, database: Database, cb: (token: DB.OAuthRequest | undefined)=>void): void {
    DB.clientsGet.getRemoteSession(database, clientId, (session)=>{
        if(!session){
            console.error("User not logged in.");
            return cb(undefined);
        }

        DB.clientsGet.getClientOAuth2Token(database, clientId, (req)=>{
            if(req){
                if(new Date().getTime()/1000 > (req.createdOn + req.expireAt)){
                    console.log("expired token. refreshing.");
                    API.gitlab.postRefreshedAccessToken(req.refreshToken, (token)=>{
                        if(!token){
                            console.error("Failed to refresh token.");
                            return cb(undefined);
                        }
                        DB.clientsSet.addOAuth2Request(database, {refreshToken: token.refreshToken, accessToken: token.accessToken, origin: "gitlab", clientId: clientId, createdOn: token.createdAt, expireAt: token.expiry}, (new_token)=>{
                            if(!new_token){
                                console.error("Failed to refresh token.");
                                return cb(undefined);
                            }
                            console.log("Token refreshed.");
                            cb(new_token);
                        })
                    })
                }else{  
                    console.log("Token valid");
                    cb(req);
                }
            }else{
                console.error("User not logged in.");
                cb(undefined);
            }
            
        })
    })
}