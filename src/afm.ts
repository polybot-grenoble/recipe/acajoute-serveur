import { Database } from 'sqlite3';
import * as DB from './db/DB.js';
import * as API from './api/API.js';
import { existsSync, writeFile, readFile, rm, mkdirSync} from 'fs';

/**
 * Classe abstraite décrivant les fonctions pour stocker le fichier du projet.
 */
export abstract class AFM {

    protected database: Database;

    constructor(database: Database){
        this.database = database;
    }

    /**
     * Sauvegarde des données du projet dans un fichier.
     * @param id Id du projet (base de données).
     * @param data Données du projet en JSON.
     * @param commit Message lié au commit.
     * @param cb Requête réussie si vide.
     */
    abstract save(id: string, data: string, commit: string, cb: resCallback): void;

    /**
     * Charge les données d'un projet.
     * @param id Id du projet.
     * @param cb Données du projet.
     */
    abstract load(id: string, cb: fileCallback): void;

    /**
     * Efface les données d'un projet. Ne met pas à jour la base de données.
     * @param id Id du projet.
     * @param cb Requête réussie si vide.
     */
    abstract delete(id: string, cb: resCallback): void;

    /**
     * Efface les données expirés des projets.
     */
    abstract clear(): void;

}

export class LAFM extends AFM {

    constructor(database: Database){
        super(database);
    }

    save(id: string, data: string, commit: string, cb: resCallback): void {
        if(!id) return cb("Specify project id.");
        if(!data) return cb("Specify project data.");
        if(!existsSync(<string>process.env.STORAGE_PATH)) mkdirSync(<string>process.env.STORAGE_PATH);
        let path = process.env.STORAGE_PATH + id + ".json";
        writeFile(path, data, "utf-8", (err)=>{
            if(err) return cb(`Could not write file : ${err}`);
            cb(undefined);
        })
    }

    load(id: string, cb: fileCallback): void {
        if(!id) return cb("Specify project id.", undefined);
        let path = process.env.STORAGE_PATH + id + ".json";
        if(!existsSync(path)) return cb("Project data not found.", undefined);
        readFile(path, "utf-8", (err, data)=>{
            if(err) return cb(`Could not get file write : ${err}`, undefined);
            cb(undefined, data);
        })
    }

    delete(id: string, cb: resCallback): void {
        if(!id) return cb("Specify project id.");
        let path = process.env.STORAGE_PATH + id + ".json";
        if(!existsSync(path)) return cb("Project data not found.");
        rm(path, (err)=>{
            if(err) return cb(`Could not delete file : ${err}`);
            cb(undefined);
        })
    }

    clear(): void {

    }

}

export class GAFM extends AFM {

    private accessToken: string;

    constructor(accessToken: string, database: Database){
        super(database);
        this.accessToken = accessToken;
    }
    
    save(id: string, data: string, commit: string, cb: resCallback): void {
        if(!id) return cb("Specify project metadata.");

        DB.project.getProject(this.database, id, (project)=>{
            if(!project) return cb("Project not found in Database.");
            if(project.origin != "gitlab" || !project.repoId) return cb("Project does not match required parameters.");
            let path = project.name  + ".json";

            //Get repo data from api
            API.gitlab.getProject(this.accessToken, project.repoId, (git)=>{
                if(!git || !project.repoId){
                    return cb("Could not get project from api.");
                }

                API.gitlab.postProjectFile(this.accessToken, project.repoId, path, git.defaultBranch, commit, data, (res)=>{
                    if(!res) return cb(undefined); //Worked.
                    
                    API.gitlab.updateProjectFile(this.accessToken, <number>project.repoId, path, git.defaultBranch, commit, data, (res)=>{
                        if(!res){
                            return cb("Could not save project.");
                        }
                        cb(undefined);
                    })
                })
            })
        })

    }

    load(id: string, cb: fileCallback): void {
        DB.project.getProject(this.database, id, (project)=>{
            if(!project) return cb("Could not load project from database.", undefined);
            if(project.origin != "gitlab" || !project.repoId) return cb("Project does not match required parameters.", undefined);
            let path = project.name  + ".json";

            API.gitlab.getProject(this.accessToken, project.repoId, (git)=>{
                if(!git || !project.repoId){
                    return cb("Could not get project from api.", undefined);
                }

                API.gitlab.getProjectFile(this.accessToken, project.repoId, path, git.defaultBranch, (err, data)=>{
                    cb(err,data);
                })
            })
        })
    }
    
    delete(id: string, cb: resCallback): void {
        DB.project.getProject(this.database, id, (project)=>{
            if(!project) return cb("Could not load project from database.");
            if(project.origin != "gitlab" || !project.repoId) return cb("Project does not match required parameters.");
            let path = project.name  + ".json";

            API.gitlab.getProject(this.accessToken, project.repoId, (git)=>{
                if(!git || !project.repoId){
                    return cb("Could not get project from api.");
                }

                API.gitlab.deleteProjectFile(this.accessToken, project.repoId, path, git.defaultBranch, "Fichier efface.", (err)=>{
                    cb(err);
                })
            })
        })
    }

    clear(): void {

    }

}

export type fileCallback = (err: string | undefined, data: string | undefined) => void;
export type resCallback = (err: string | undefined) => void;