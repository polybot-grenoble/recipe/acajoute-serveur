type id = string;

type celluleType = 
    "constructeur" |
    "variable"    |
    "constante"   ;

type arbreFlowType =
    'normal' | 
    'retour' |
    'racine' | 
    'if' | 
    'while' | 
    'foreach' | 
    'switch';

type cellule = {
    adresse : id;
    type : celluleType;
    fonction : arbreFlowType;
    refVariable? : id;
    valeur? : string;
    fils : id[];

};

type AbsSynTree = {
    
    racine: id;

    cellules:{
        [clefs: id] : cellule;
    }
};

function feuilleVariableToCellule(FeuilleActuel:__Feuille) : cellule{
    let newCellule : cellule = {
        adresse : FeuilleActuel.id,
        type : 'variable',
        fonction : 'normal',
        /***/
        fils : ['null'],

        /*Cas variable*/
        refVariable : FeuilleActuel.ref,
    };

    return newCellule;
}

function feuilleOperateurToCellule(FeuilleActuel: __Feuille, operateur : string) : cellule{
    let newCellule : cellule = {
        adresse : FeuilleActuel.id,
        type : 'constructeur',
        fonction : 'normal',
        /***/
        
        /*Cas operateur*/
        valeur : operateur,
        fils : ['null'],
    };

    return newCellule;
}
