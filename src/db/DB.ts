import { Database } from "sqlite3";
import * as clientsSet from "./client/clientSet.js";
import * as clientsRm from "./client/clientDelete.js";
import * as clientsGet from "./client/clientGet.js";
import * as project from "./project/project.js";

export {clientsSet, clientsGet, clientsRm, project};

/**
 * Ouvre une base de données SQLite et l'initialise au besoin.
 * @param file Fichier base de données SQLite.
 */
export function openClientDB (file: string): Database {
    let db = new Database(file);
    initDB(db);
    return db;
}

/**
 * Initialise une base de données SQLite.
 * @param db La base de données contenant les clients.
 */
function initDB (db: Database): void {
    clientsSet.initClientsTable(db);
    clientsSet.initOAuthTable(db);
    clientsSet.initRemoteSessionTable(db);
    project.initProjectTable(db);
}

/** Type qui décrit les données d'un client dans la base de données. */
export type Client = {
    id: string;
    name?: string;
    lastLogin: number;
    createdOn: number;
}

/** Type qui décrit les données d'une session de connexion dans la base de données. */
export type RemoteSession = {
    clientId: string;
    remoteClientId: string;
    origin: origin;
    createdOn: string;
}

/** Type qui décrit les données d'une requête oauth dans la base de données. */
export type OAuthRequest = {
    clientId: string;
    accessToken: string;
    refreshToken: string;
    createdOn: number;
    expireAt: number;
    origin: origin;
}

/** Type qui décrit les données d'un projet. */
export type Project = {
    id: string;
    name: string;
    clientId: string;
    createdOn: number;
    lastAccessed: number;
    origin: origin;
    repoId?: number;
}

/** Type qui décrit les origines possibles d'une session. */
export type origin = "local" | "gitlab";