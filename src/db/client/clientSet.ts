import { Database } from "sqlite3";
import { Client, OAuthRequest, origin } from "../DB";
import { getClient, getRemoteSession } from "./clientGet";

/**
 * Ajoute un utilisateur dans la base de données.
 * @param db La base de données contenant les clients.
 * @param client l'uuid du client.
 */
export function addClient (db: Database, id: string, cb: (id: Client | undefined) => void): void {
    let time = new Date().getTime();
    let name = randomName();

    db.run('INSERT INTO Clients (id,name,lastAccessed,createdAt) VALUES (?,?,?,?)', [id,name,time,time], (err)=>{
        if(err){
            console.error('Error while adding user: ', err);
            return cb(undefined);
        }
        cb({id: id, name: name, lastLogin: time, createdOn: time});
    });
}

/**
 * Modifie le nom d'un utilisateur.
 * @param db La base de données contenant les clients.
 * @param id Id du client.
 * @param name Nouveau nom du client.
 * @param cb Vrai si la requête a aboutie.
 */
export function setName(db: Database, id: string, name: string, cb: (result: boolean) => void): void {
    db.run('UPDATE Clients SET name=? WHERE id=?', [name, id], (err)=>{
        if(err){
            console.error('Error while setting user\'s name: ', err);
            return cb(false);
        }
        cb(true);
    })
    updateUserAccessTime(db, id);
}

/**
 * Met à jour la date du dernier accès de l'utilisateur.
 * @param db La base de données contenant les clients.
 * @param id Id du client.
 */
export function updateUserAccessTime(db: Database, id: string): void {
    db.run('UPDATE Clients SET lastAccessed=? WHERE id=?', [new Date().getTime()/1000,id], (err)=>{
        if(err) console.error('Error while updating user\'s last access: ', err);
    })
}

/**
 * Ajoute une session de connexion à un utilisateur.
 * @param db La base de données contenant les clients.
 * @param clientId Id du client.
 * @param remoteClientId Id lié à l'origine de la session.
 * @param origin Origine de la session. (gitlab)
 * @param cb Vrai si la requête a aboutie.
 */
export function addRemoteSession(db: Database, clientId: string, remoteClientId: string, origin: origin, cb: (res: boolean)=>void): void {
    getClient(db, clientId, (res)=>{
        if(!res){
            console.error("DATABASE: Error while adding remote session: User does not exists.");
            return cb(false);
        }

        getRemoteSession(db, clientId, (session)=>{
            if(session && session.origin == origin){
                console.info("DATABASE: Remote session already exists.");
                return cb(false);
            }

            db.run('INSERT INTO RemoteSession (clientId,remoteClientId,origin,createdOn) VALUES (?,?,?,?)', [clientId,remoteClientId,origin,new Date().getTime()], (err)=>{
                if(err){
                    console.error('Error while adding remote session: ', err);
                    return cb(false);
                }
                cb(true);
            });
        })
    })
}

/**
 * Ajoute une requête dans la base de données.
 * @param db La base de données contenant les clients.
 * @param client l'uuid du client.
 * @param token_id c'est dans le nom.
 */
export function addOAuth2Request (db: Database, request: OAuthRequest, cb: (request: OAuthRequest | undefined)=>void): void {
    db.run('INSERT INTO OAuth (clientId, accessToken, refreshToken, createdOn, expireAt, origin) VALUES (?,?,?,?,?,?)', [request.clientId, request.accessToken, request.refreshToken, request.createdOn, request.expireAt, request.origin], (err)=>{
        if(err){
            console.error("Error while adding oauth request: ", err);
            return cb(undefined);
        }
        cb(request);
    })
}

/**
 * Initialise la table Clients de la base de données.
 * @param db La base de données contenant les clients.
 */
export function initClientsTable (db: Database): void {
    db.run("CREATE TABLE IF NOT EXISTS Clients (id varchar(36) PRIMARY KEY NOT NULL, name varchar(64), lastAccessed INTEGER, createdAt INTEGER)", (err)=>{
        if(err) console.error('Error during clients table init: ', err);
    });
}

/**
 * Initialise la table RemoteSession de la base de données.
 * @param db La base de données contenant les clients.
 */
export function initRemoteSessionTable (db: Database): void {
    db.run("CREATE TABLE IF NOT EXISTS RemoteSession (clientId varchar(36) NOT NULL, remoteClientId varchar(64) PRIMARY KEY NOT NULL, origin varchar(32) NOT NULL, createdOn INTEGER, FOREIGN KEY (clientId) REFERENCES Clients(id))", (err)=>{
        if(err) console.error('Error during remote session table init: ', err);
    });
}

/**
 * Initialise la table OAuth de la base de données.
 * @param db La base de données contenant les clients.
 */
export function initOAuthTable(db: Database): void {
    db.run("CREATE TABLE IF NOT EXISTS OAuth (clientId varchar(36) NOT NULL, accessToken varchar(64) PRIMARY KEY NOT NULL UNIQUE, refreshToken varchar(64) NOT NULL UNIQUE, createdOn INTEGER, expireAt INTEGER, origin varchar(32) NOT NULL, FOREIGN KEY (clientId) REFERENCES Clients(id))", (err)=>{
        if(err) console.error('Error during oauth table init: ', err);
    });
}

/**
 * Genère un nom aléatoire.
 * @returns Nom aléatoire.
 */
function randomName(): string {
    const list = ["Michel", "Paul"];
    return list[Math.floor(Math.random()*list.length)]
}