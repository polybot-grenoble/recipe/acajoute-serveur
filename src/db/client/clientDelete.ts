import { Database } from "sqlite3";
import { origin } from "../DB";

/**
 * Fonction qui supprime un utilisateur.
 * @param db La base de données contenant les clients.
 * @param client l'uuid du client.
 * @param cb Vrai si la suppresion fonctionne.
 */
export function rmClient (db: Database, id: string, cb: (res: boolean)=>void): void {
    db.run('DELETE FROM Clients WHERE id=?', [id], (err)=>{
        if(err){
            console.error('Error while removing client: ', err);
            return cb(false);
        }
        cb(true);
    })
}

/**
 * Fonction qui supprime toutes les requêtes de la base de données appartenant à un client.
 * @param db La base de données contenant les clients.
 * @param clientId l'uuid du client.
 * @param cb Vrai si la suppresion fonctionne.
 */
export function rmAllOAuth2Requests (db: Database, clientId: string, cb: (res: boolean)=>void): void {
    db.run('DELETE FROM OAuth WHERE clientId=?', [clientId], (err)=>{
        if(err){
            console.error('Error while removing tokens: ', err);
            return cb(false);
        }
        cb(true);
    })
}

/**
 * Fonction qui supprime les sessions de connexion d'un utilisateur en fonction de leur origine.
 * @param db La base de données contenant les clients.
 * @param clientId Id du client.
 * @param origin Origine de la session. (gitlab).
 * @param cb Vrai si la suppression fonctionne.
 */
export function rmRemoteSession(db: Database, clientId: string, origin: origin, cb: (res: boolean)=>void): void {
    db.run('DELETE FROM RemoteSession WHERE clientId=? AND origin=?', [clientId, origin], (err)=>{
        if(err){
            console.error('Error while removing user\'s remote session: ', err);
            return cb(false);
        }
        cb(true);
    })
}

/**
 * Fonction qui supprime les jetons oauth expirés d'un utilisateur.
 * @param db La base de données contenant les clients.
 * @param clientId Id du client.
 * @param cb Vrai si la suppresion fonctionne.
 */
export function rmExpiredOAuthToken(db: Database, clientId: string, cb: (res: boolean)=>void): void {
    db.run('DELETE FROM OAuth WHERE clientId=? AND ? > (createdOn + expireAt)', [clientId, new Date().getTime()/1000], (err)=>{
        if(err){
            console.error('Error while removing expired tokens: ', err);
            return cb(false);
        }
        cb(true);
    })
}