import { Database } from "sqlite3";
import { Client, OAuthRequest, RemoteSession } from "../DB";
import { updateUserAccessTime } from "./clientSet";

/**
 * Retourne les données d'un client.
 * @param db La base de données contenant les clients.
 * @param id Id du client.
 * @param cb Contient les données du client. Vide si la requête n'a pas aboutie.
 */
export function getClient(db: Database, id: string, cb: (res: Client |undefined)=>void): void {
    db.get("SELECT * from Clients WHERE id=?", [id], (err, row)=>{
        if(err){
            console.log("Error while getting client: ", err);
            return cb(undefined);
        }
        if(!row) return cb(undefined);
        cb((row as Client));
    });
    updateUserAccessTime(db, id);
}

/**
 * Retourne les données d'une session de connexion.
 * @param db La base de données contenant les clients.
 * @param clientId Id du client.
 * @param cb Contient les données de la session. Vide si la requête n'a pas aboutie.
 */
export function getRemoteSession(db: Database, clientId: string, cb: (res: RemoteSession | undefined)=>void): void {
    db.get("SELECT * from RemoteSession WHERE clientId=?", [clientId], (err, row)=>{
        if(err){
            console.log("Error while getting remote session: ", err);
            return cb(undefined);
        }
        if(!row) return cb(undefined);
        cb((row as RemoteSession));
    });
    updateUserAccessTime(db, clientId);
}

/**
 * Retourne le jeton OAuth2 le plus récent d'un client.
 * @param db La base de données contenant les clients.
 * @param id l'uuid du client.
 * @param cb Contient les doonées du jeton. Vide si la requête n'a pas aboutie.
 */
export function getClientOAuth2Token (db: Database, id: string, cb: (request: OAuthRequest | undefined)=>void): void {
    db.get("SELECT * FROM OAuth WHERE clientId=? ORDER BY createdOn DESC", [id], (err,row)=>{
        if(err){
            console.error("DATABASE: Failed to get user oauth token.");
            return cb(undefined);
        }
        cb((row as OAuthRequest));
    });
}