import { Database } from "sqlite3";
import { Project, origin } from "../DB";


/**
 * Initialise la table Project de la base de données.
 * @param db 
 */
export function initProjectTable(db: Database): void {
    db.run("CREATE TABLE IF NOT EXISTS Project (id varchar(36) PRIMARY KEY NOT NULL UNIQUE, name varchar(128) NOT NULL, clientId varchar(36) NOT NULL, createdOn INTEGER, lastAccessed INTEGER, origin varchar(32) NOT NULL, repoId INTEGER, FOREIGN KEY (clientId) REFERENCES Clients(id))", (err)=>{
        if(err) console.error('Error during oauth table init: ', err);
    });
}

/**
 * Met à jour la date du dernier accès du projet.
 * @param db Base de données
 * @param id Id du projet.
 */
export function updateProjectAccessTime(db: Database, id: string): void {
    db.run('UPDATE Project SET lastAccessed=? WHERE id=?', [new Date().getTime()/1000,id], (err)=>{
        if(err) console.error('Error while updating project\'s last access: ', err);
    })
}

/**
 * Met à jour l'origine du projet.
 * @param db Base de données.
 * @param id Id du projet.
 * @param origin Origine du projet (local,gitlab).
 * @param repoId Id du repo (pour gitlab).
 * @param cb Vrai si la requête a aboutie.
 */
export function updateProjectOrigin(db: Database, id: string, origin: origin, repoId: number, cb: (done: boolean | undefined) => void): void {
    db.run('UPDATE Project SET origin=?, repoId=? WHERE id=?', [origin, repoId, id], (err)=>{
        if(err){
            console.error('Error while updating project\'s origin: ', err);
            return cb(false);
        }
        cb(true);
    });
    updateProjectAccessTime(db, id);
}

/**
 * Ajoute un projet dans la base de données.
 * @param db Base de données.
 * @param id Id du projet.
 * @param name Nom du projet.
 * @param clientId Id du client.
 * @param origin Origine du projet (local,gitlab).
 * @param cb Retourne les données liées au projet. Vide si la requête n'a pas aboutie.
 */
export function addProject(db: Database, id: string, name: string, clientId: string, origin: origin, cb: (project: Project | undefined) => void): void {
    let t: number = new Date().getTime()/1000;
    db.run("INSERT INTO Project (id,name,clientId,createdOn,lastAccessed,origin) VALUES (?,?,?,?,?,?)", [id, name, clientId, t, t, origin], (err)=>{
        if(err){
            console.error('Error while adding project: ', err);
            return cb(undefined);
        }
        cb({id: id, name: name, clientId: clientId, createdOn: t, lastAccessed: t, origin: origin});
    })
}

/**
 * Retourne les données liées à un projet.
 * @param db Base de données.
 * @param id Id du projet.
 * @param cb Retourne les données liées au projet. Vide si la requête n'a pas aboutie.
 */
export function getProject(db: Database, id: string, cb: (project: Project | undefined) => void): void {
    db.get("SELECT * from Project WHERE id=?", [id], (err, row)=>{
        if(err){
            console.log("Error while getting client: ", err);
            return cb(undefined);
        }
        if(!row) return cb(undefined);
        cb((row as Project));
    });
    updateProjectAccessTime(db, id);
}

/**
 * Supprime un projet.
 * @param db Base de données.
 * @param id Id du projet.
 * @param cb Vrai si la requête a aboutie.
 */
export function deleteProject(db: Database, id: string, cb: (err: boolean | undefined) => void): void {
    db.run('DELETE FROM Project WHERE id=?', [id], (err)=>{
        if(err){
            console.error('Error while removing project\'s data.', err);
            return cb(false);
        }
        cb(true);
    })
}

/**
 * Retourne les projets liés à un utilisateur.
 * @param db Base de données.
 * @param clientId Id du client.
 * @param cb Retourne la liste des projets. Vide si la requête n'a pas aboutie.
 */
export function getProjects(db: Database, clientId: string, cb: (projects: Project[] | undefined) => void): void {
    db.all("SELECT * from Project WHERE clientId=?", [clientId], (err, rows)=>{
        if(err){
            console.log("Error while getting client: ", err);
            return cb(undefined);
        }
        if(!rows) return cb(undefined);
        cb((rows as Project[]));
    });
}

/**
 * Supprime les projets expirés (locaux, 7 jours sans utilisation).
 * @param db Base de données.
 * @param cb Vrai si la requête a aboutie.
 */
export function cleanProjects(db: Database, cb: (done: boolean | undefined) => void): void {

}